package ru.tsc.karbainova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class TaskBindByIdProject extends AbstractCommand {
    @Override
    @NotNull
    public String name() {
        return "task-bind-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        SessionDTO session = serviceLocator.getSession();
        serviceLocator.getProjectEndpoint().taskBindByIdProject(session, projectId, taskId);
    }

}
