package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.endpoint.UserDTO;

import java.util.List;

public interface IUserRepository {
    List<UserDTO> findAll();

    UserDTO add(UserDTO user);

    void addAll(List<UserDTO> tasks);

    UserDTO findById(String id);

    UserDTO findByLogin(String login);

    UserDTO findByEmail(String email);

    UserDTO removeUser(UserDTO user);

    UserDTO removeById(String id);

    UserDTO removeByLogin(String login);

    void clear();

}
