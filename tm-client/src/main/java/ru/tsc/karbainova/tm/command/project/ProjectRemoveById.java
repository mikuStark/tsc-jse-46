package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class ProjectRemoveById extends AbstractCommand {
    @Override
    public String name() {
        return "remove-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by index";
    }

    @Override
    public void execute() {

        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        SessionDTO session = serviceLocator.getSession();
        serviceLocator.getProjectEndpoint().removeProjectById(session, projectId);
    }

}
