package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.dto.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void clearUser(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByUserId(session.getUserId());
    }

    @WebMethod
    public void addAllUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "users") List<UserDTO> users) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().addAll(users);
    }

    @WebMethod
    public List<UserDTO> findAllUser(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    public UserDTO findByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    public UserDTO removeUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "user") UserDTO user
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().removeUser(user);
    }

    @WebMethod
    public UserDTO lockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().lockUserByLogin(login);
    }

    @WebMethod
    public UserDTO unlockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().unlockUserByLogin(login);
    }
}
