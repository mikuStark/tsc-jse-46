package ru.tsc.karbainova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDTORepository extends AbstractDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    public @NotNull ProjectDTO findById(@Nullable final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId",
                        ProjectDTO.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class).getResultList();
    }

    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ProjectDTO findByIdUserId(@Nullable final String userId, @Nullable final String id) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId", ProjectDTO.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final ProjectDTO entity) {
        ProjectDTO reference = entityManager.getReference(ProjectDTO.class, entity.getId());
        entityManager.remove(reference);
    }

    @Override
    public void removeById(@Nullable final String id) {
        ProjectDTO reference = entityManager.getReference(ProjectDTO.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIdAndUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int getCountByUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

}

