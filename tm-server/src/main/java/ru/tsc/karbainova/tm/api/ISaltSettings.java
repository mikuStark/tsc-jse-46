package ru.tsc.karbainova.tm.api;

import lombok.NonNull;

public interface ISaltSettings {
    @NonNull
    Integer getPasswordIteration();

    @NonNull
    String getPasswordSecret();
}
