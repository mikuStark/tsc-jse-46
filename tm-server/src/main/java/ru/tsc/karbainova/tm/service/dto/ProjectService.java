package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractOwnerService<ProjectDTO> implements IProjectService {

    public ProjectService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<ProjectDTO> collection) {
        if (collection == null) return;
        for (ProjectDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public ProjectDTO add(ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeByIdAndUserId(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public ProjectDTO updateById(@NonNull String userId, @NonNull String id,
                                 @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDTO project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        try {
            project.setName(name);
            project.setDescription(description);
            final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NonNull
    @SneakyThrows
    public ProjectDTO findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }
}
