package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.repository.model.ISessionRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.api.service.model.ISessionServiceModel;
import ru.tsc.karbainova.tm.component.Bootstrap;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AccessDeniedException;
import ru.tsc.karbainova.tm.exception.AccessForbiddenException;
import ru.tsc.karbainova.tm.exception.empty.EmptyLoginException;
import ru.tsc.karbainova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.karbainova.tm.exception.empty.EmptySessionNlException;
import ru.tsc.karbainova.tm.exception.empty.EmptyUserNotFoundException;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.repository.model.SessionRepository;
import ru.tsc.karbainova.tm.service.PropertyService;
import ru.tsc.karbainova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

public class SessionService extends AbstractService<Session> implements ISessionServiceModel {

    public SessionService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = serviceLocator.getUserServiceModel().findByLogin(login);
        if (user == null) throw new EmptyUserNotFoundException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    public Session add(Session session) {
        if (session == null) throw new EmptySessionNlException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Session open(String login, String password) {
//        final boolean check = checkDataAccess(login, password);
//        if (!check) throw new EmptyLoginOrPasswordException();
//        if (check) throw new EmptyLoginOrPasswordException();
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = serviceLocator.getUserServiceModel().findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            sign(session);
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Session sign(Session session) {
        if (session == null) throw new EmptySessionNlException();
        session.setSignature(null);
        IPropertyService propertyService = new PropertyService();
        final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Session> getListSessionByUserId(String userId) {
        return findAll()
                .stream()
                .filter(s -> s.getUser().getId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void validate(Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            final ISessionRepository repository = new SessionRepository(entityManager);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void validate(Session session, Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        final String userId = session.getUser().getId();
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = session.getUser();
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    @SneakyThrows
    public void close(Session session) {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            sessionRepository.removeById(session.getId());
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void closeAll(Session session) {
        validate(session);
        List<Session> sessions = findAll().stream().filter(s -> s.getUser().getId().equals(session.getUser().getId())).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

    @Override
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<Session> sessions = findAll().stream().filter(s -> s.getUser().getId().equals(userId)).collect(Collectors.toList());
        sessions.forEach(this::close);
    }
}
